import requests
import json as JSON
import datetime
import sys

def geocoding(address):
    # Header dictionary (for token)
    headerDict = {
        "Authorization" : 'YOUR-TOKEN'
    }
    if headerDict['Authorization'] == 'YOUR-TOKEN': sys.exit("Error: enter Your token in the headerDict")

    # URL of the request
    geocoding_pattern = 'http://api.navitia.io/v1/places?q={address}&type[]=address'
    geocoding_url = geocoding_pattern.format(address=address)

    # Make the request
    r = requests.get(geocoding_url, headers=headerDict)

    if r.status_code == 200:
        # Get returned data of the request
        returned_data = r.text
        # Transform JSON data to Python object
        data = JSON.loads(returned_data)
        return data['places'][0]['address']['coord']
    else:
        print("error: status_code={}".format(r.status_code))

def next_datetime():
    """Return next monday at 9am datetime string
        Needs the `python-dateutil` module: pip install python-dateutil
    
    Returns:
        [str] -- next monday date: YYYY-MM-DDThh:mm:ss
    """

    now = datetime.datetime.now()
    day = 0 # next monday
    now_9am = now.replace(hour=9,minute=0,second=0,microsecond=0) # at 9am
    next_monday = now_9am + datetime.timedelta(days=(day-now_9am.weekday()+7)%7)
    return next_monday.replace(microsecond=0).isoformat()

def best_journey(start, end):
    # Header dictionary (for token)
    headerDict = {
        "Authorization" : 'YOUR-TOKEN'
    }
    if headerDict['Authorization'] == 'YOUR-TOKEN': sys.exit("Error: enter Your token in the headerDict")

    # URL of the request
    journey_pattern = 'http://api.navitia.io/v1/journeys?from={from_lon};{from_lat}&to={to_lon};{to_lat}&datetime={datetime}&datetime_represents=arrival'
    journey_url = journey_pattern.format(
        from_lon=start['lon'],
        from_lat=start['lat'],
        to_lon=end['lon'],
        to_lat=end['lat'],
        datetime=next_datetime(),
    )

    # Make the request
    r = requests.get(journey_url, headers=headerDict)

    if r.status_code == 200:
        # Get returned data of the request
        returned_data = r.text
        # Transform JSON data to Python object
        data = JSON.loads(returned_data)
        # Get the best journey (the one with the LATER departure date_time)
        best_journey = None
        for journey in data['journeys']:
            if best_journey == None or best_journey['departure_date_time'] < journey['departure_date_time']:
                best_journey = journey
        return best_journey
    else:
        print("error: status_code={}".format(r.status_code))

if __name__ == '__main__':
    # Test geocoding
    address = '14 Boulevard Copernic, 77420 Champs-sur-Marne'
    coord = geocoding(address)
    print(coord) # {'lat': '48.841076', 'lon': '2.586311'}
    # Test best_journey
    start = {'lat': '48.841076', 'lon': '2.586311'}
    end = {'lat': '48.8573352', 'lon': '2.347556'}
    journey = best_journey(start, end)
    print(journey['duration'])
